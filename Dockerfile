FROM php:8.3-apache
COPY src/ /var/www/html/
RUN apt-get update \
    && apt install -y libgs9-common\
    && apt-get clean && rm -rf /var/lib/apt/lists/* \
    && docker-php-ext-install pdo_mysql
EXPOSE 80
CMD ["apache2-foreground"]
